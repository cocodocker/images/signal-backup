#! /bin/bash
# This will find the latest .backup in the input folder and decode it to /output as a .db file
mkdir output 
db=$(find /input -type f -printf '%T@ %p\n' | sort -n | tail -1 | cut -f2- -d" ")
signal-backup-decode -f -o output --password-file /run/secrets/signal_pass ${db}
echo "backup decoded"
IFS=$'\n'
# Assuming CONV_FILE may or may not contain multiple paths separated by special
# character ":", we split the paths into an array
# For each file of the array, we loop over every line and launch signal_backup.py
# with the value (the name of a conversation from signal)
for conv_file in $(sed -r 's/:/\n/g' <(echo $CONV_FILE)); do 
	conv_name=$(basename $conv_file);
	echo "conversation:" $conv_name;
	for c in $(cat $conv_file); do  
		cd /opt/signal-backup;
		python3 ./signal_backup.py --db output/signal_backup.db --attachment output/attachment -cn "$c" -y $YOUR_NAME --o $conv_name/user/pages/$conv_name;
		cd $conv_name/user/pages/$conv_name;

		sed -i 's/href="bootstrap/href="..\/bootstrap/g' ./*/*/index.html;
		sed -i 's/href="output/href="..\/output/g' ./*/*/index.html;
		sed -i 's/src="output/src="..\/output/g' ./*/*/index.html;
	done
done

for f in  $(find ./*/*/ -name index.html); do
	d=$(dirname $f);
	cp -n /opt/signal-backup/templates/blog.md $d/;
done
echo "backup done"

