FROM rust

RUN cargo install signal-backup-decode && apt-get install -y python3 && mkdir -p /input /output /config

COPY src /opt/signal-backup
RUN chmod +x /opt/signal-backup/signal_backup.py && chmod +x /opt/signal-backup/entrypoint.sh

ENV CONV_FILE=/config/conversations.txt
ENV YOUR_NAME="NoName"

WORKDIR /opt/signal-backup
VOLUME /input

ENTRYPOINT /opt/signal-backup/entrypoint.sh
